# IPInfo

Basic IP and Hostname checker

Returns external IPs and Hostnamse for the client when called in JSON format

Parses request Headers `X-Forwarded-For` and `X-Real-IP` as well as `RemoteAddr` and returns a JSON list if multiple IPs are discovered.

Private and Loopback addresses are ignored and not returned

## Quickstart

### Local testing

```console
podman run -p 8080 registry.gitlab.com/jandourekjaroslav/ipinfo:latest
curl -H "X-Forwarded-For: 1.1.1.1" localhost:8080
```

### Deployment

1. Start the service as a container
   To change the port from the default 8080 pass `SERVICE_PORT` as ENV
   ```console
   podman run -d -p 8080 registry.gitlab.com/jandourekjaroslav/ipinfo:latest
   ```
1. Create an nginx conf file to direct traffic to it

   ```nginx
   server {
       server_name ip.yourhostnamehere.com www.ip.yourhostnamehere.com;



       location / {
           proxy_set_header X-Real-IP $remote_addr;
           proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
           proxy_pass http://localhost:8080;
       }

   listen [::]:80 ;
   listen 80;

   }
   ```

1. Test your config and restart nginx

   ```console
   nginx -t && systemctl restart nginx
   ```

1. Test it out
   ```console
   curl http://ip.yourhostnamehere.com
   ```
   The expected output should look something like this:
   ```json
   [
     {
       "ip": "1.1.1.1",
       "hostnames": ["one.one.one.one."]
     }
   ]
   ```
