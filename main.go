package main

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
)

type ipHandler struct{}
type ipInfo struct {
	IP        string   `json:"ip"`
	Hostnames []string `json:"hostnames"`
}
type ipsInfo []ipInfo

func (h *ipHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	ip_headers := r.Header.Values("X-Forwarded-For")
	ips := make(map[string]bool)
	var infos ipsInfo
	remoteip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err == nil && !net.ParseIP(remoteip).IsPrivate() && !net.ParseIP(remoteip).IsLoopback() {
		ips[remoteip] = true
	}
	if len(r.Header.Get("X-Real-IP")) > 0 {
		ips[r.Header.Get("X-Real-Ip")] = true
	}
	for _, v := range ip_headers {
		for _, u := range strings.Split(v, ",") {
			ips[u] = true
		}
	}
	for ip, _ := range ips {
		hostnames, _ := net.LookupAddr(ip)
		infos = append(infos, ipInfo{IP: ip, Hostnames: hostnames})
	}
	response, err := json.MarshalIndent(infos, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(response)
}

func main() {
	http.Handle("/", new(ipHandler))
	port := os.Getenv("SERVICE_PORT")
	if len(port) == 0 {
		port = "8080"
	}
	log.Println("Starting server")
	log.Fatal(http.ListenAndServe(":"+port, nil))

}
