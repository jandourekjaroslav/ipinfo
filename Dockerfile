FROM --platform=${BUILDPLATFORM:-linux/amd64} docker.io/golang:1.21-alpine as build

WORKDIR $GOPATH/src/gitlab.com/jandourekjaroslav/ipinfo

COPY go.mod go.mod
COPY main.go main.go
RUN go mod download

RUN go build .


FROM --platform=${TARGETPLATFORM:-linux/amd64} docker.io/alpine:3.18

COPY --from=build /go/src/gitlab.com/jandourekjaroslav/ipinfo/ipinfo /ipinfo
RUN adduser -u 1000 -D runner
RUN chown runner /ipinfo
USER 1000

EXPOSE 8080/tcp

CMD /ipinfo
